module EventRegistrationsHelper
  def is_registered(event, user)
    if logged_in?
      user_registration = User.joins(:event_registrations).where('event_registrations.event_id = ? AND event_registrations.user_id = ?', event.id, current_user.id)
      if user_registration.blank?
        return false
      else 
        return true
      end
    end
  end

  def add_registration(u_id, e_id)
    user_registration = User.joins(:event_registrations).where('event_registrations.event_id = ? AND event_registrations.user_id = ?', e_id, u_id)
    if user_registration.blank?
      event_registration = EventRegistration.new(:user_id => u_id, :event_id => e_id)
      event_registration.save
      flash[:success] = "Event Registration Successful."
    end
  end

  def delete_user_registration(u_id, e_id)
    user_registration = EventRegistration.find_by(user_id: u_id, event_id: e_id)
    if not user_registration.blank?
      user_registration.delete
      flash[:success] = "Event Registration Canceled."
    end
  end

end
