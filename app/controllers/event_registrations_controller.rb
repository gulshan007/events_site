class EventRegistrationsController < ApplicationController

  def register_user
    if logged_in?
      add_registration(current_user.id,params[:event_id])
      redirect_to event_path(:id => params[:event_id])
    else
      session[:return_to] ||= event_path(:event_id => params[:event_id])
      redirect_to login_path
    end
  end

  def delete_registration
    if logged_in?
      delete_user_registration(current_user.id,params[:event_id])
      redirect_to event_path(:id => params[:event_id])
    else
      session[:return_to] ||= event_path(:event_id => params[:event_id])
      redirect_to login_path
    end
  end

end
