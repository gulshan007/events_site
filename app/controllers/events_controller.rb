class EventsController < ApplicationController

  def index
    @events = Event.all.order(created_at: :desc)
  end

  def show
    @event = Event.find(params[:id])
    @user = current_user
    @registered_users = User.joins(:event_registrations).where('event_registrations.event_id = ?', params[:id])
  end
  
end
