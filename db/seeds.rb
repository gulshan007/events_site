# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Event.create(name: 'Hikeathon', description: "Hike is India’s largest messaging app boasting of over 35M users. Launched on 12/12/12, Hike has continuously innovated to build India-centric features like Last Seen Privacy, Hidden Mode and Localised stickers. Over 90% of Hike’s user base is under the age of 25(!). In August 2014, Hike raised a big round of funding - $65 million - led by Tiger Global and BSB. Come join the conversation makers for a 24 Hour Hackathon!", date_time: "2015-03-21 05:00:00", venue: "Hike Office, Bangalore", ticket: 200, created_at:"2015-02-23 01:27:00")

Event.create(name: 'GainSight Happy Hack', description: "Gainsight is the first and only complete Customer Success Management solution that helps businesses reduce churn, increase up-sell and drive customer success. The company’s SaaS suite integrates with Salesforce and uses Big Data analytics to evaluate sales data, usage logs, support tickets, surveys and other sources of customer intelligence. Companies like Angie’s List, Castlight Health, Marketo and Informatica are using Gainsight to help their customers succeed. The Company is backed by world class investors including Salesforce Ventures, Lightspeed, Bain Capital Ventures Leads Round, Battery Ventures and Summit Partners.", date_time: "2015-04-22 05:00:00", venue: "Bangalore", ticket: 500, created_at:"2015-03-25 04:29:00")

Event.create(name: 'TookiTaki', description: "Tookitaki is a well funded marketing intelligence startup focused on audience discovery and prediction. They are building a SaaS platform for enterprises and media agencies, that will provide actionable insights on audience behavior. Their analytics are based on predictive models which combines public digital data with ROI focussed feedback loops. They have a few big names in our client roster and they are growing very fast. Really fast.", date_time: "2015-03-29 06:00:00", venue: "Online", ticket: 0, created_at:"2015-02-15 05:27:00")

Event.create(name: 'ZapStitch', description: "ZapStitch is the perfect solution for companies that want to sync and manage data on their cloud based apps seamlessly, minus the API developer. The cloud based business app, Zapstitch, aids companies migrate, synchronize and manage data between the various cloud based apps employed by them. ", date_time: "2015-05-25 05:00:00", venue: "Bangalore", ticket: 300, created_at:"2015-03-20 05:17:00")

Event.create(name: 'Simplilearn', description: "Simplilearn is world’s largest certification training provider! A pioneer in online education and training for professional certification courses, Simplilearn creates education programs, develop exams and labs that aid in cracking the certification exams. They offer a “blended model” of training to provide our customers optimal learning experience. ", date_time: "2015-02-25 05:00:00", venue: "Online", ticket: 100, created_at:"2015-02-20 01:27:00")



User.create(name: "Alex", email: "alex@example.com", gender: "M", password: "alex1234", password_confirmation: "alex1234")
User.create(name: "Bella", email: "bella@example.com", gender: "F", password: "bella1234", password_confirmation: "bella1234")
User.create(name: "Chuck", email: "chuck@example.com", gender: "M", password: "chuc1234", password_confirmation: "chuc1234")
User.create(name: "Dean", email: "dean@example.com", gender: "M", password: "dean1234", password_confirmation: "dean1234")
User.create(name: "Elsa", email: "elsa@example.com", gender: "F", password: "elsa1234", password_confirmation: "elsa1234")