class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.text :description
      t.datetime :date_time
      t.string :venue
      t.integer :ticket

      t.timestamps null: false
    end
  end
end
