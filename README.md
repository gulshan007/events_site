== README

Events Website: EventZilla


* Ruby version:		ruby 2.2.1p85 (2015-02-26 revision 49769)

* Rails version:	rails 4.2.0



Getting started:

*	git clone https://bitbucket.org/gulshan007/events_site.git
*	bundle install
*	bundle exec rake db:migrate
*	bundle exec rake db:seed
*	rails s
*	Goto http://localhost:3000 in your browser.

(Tested in Chrome)