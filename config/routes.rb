Rails.application.routes.draw do
  
  root                  'events#index'

  get     'signup'              =>  'users#new',    as:   'signup'
  post    'users'               =>  'users#create'
  #get     'users/:id'           =>  'users#show',   as:   'user'

  get     'login'               =>  'sessions#new',     as:   'login'
  post    'login'               =>  'sessions#create'
  delete  'logout'              =>  'sessions#destroy', as:   'logout'
  
  get     'events/:id'          =>  'events#show',  as: 'event'
  get     'events'              =>  'events#index'

  get     'register/:id'        =>  'event_registrations#register_user',        as: 'register_user'
  get     'deletereg/:id'       =>  'event_registrations#delete_registration', as: 'delete_registration'

end
