require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def setup
    @user = User.new( name: "Test User1", email: "testuser@example.com", gender:"M",
                      password: "foobar12", password_confirmation: "foobar12" )
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "name should be present" do
    @user.name="     "
    assert_not @user.valid?
  end

  test "email should be present" do
    @user.email="     "
    assert_not @user.valid?
  end

  test "gender should be present" do
    @user.gender="     "
    assert_not @user.valid?
  end

  test "name should not exceed 50 characters" do
    @user.name="a"*51
    assert_not @user.valid?
  end

  test "email should not exceed 250 characters" do
    @user.email="a"*251
    assert_not @user.valid?
  end

  test "gender should be 1 char long" do
    @user.gender="a"*2
    assert_not @user.valid?
  end

  test "gender should reject all other values except M or F" do
    invalid_gender_values = %w[A B C D @ # $ ! 0 23A 4]
    invalid_gender_values.each do |invalid_gender|
      @user.gender = invalid_gender
      assert_not @user.valid?, "#{invalid_gender.inspect} should be valid"
    end
  end

  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  test "email addresses should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end

  test "password should be at least 8 characters long" do
    @user.password = @user.password_confirmation = "a"*7
    assert_not @user.valid?
  end

end